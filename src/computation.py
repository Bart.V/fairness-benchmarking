from random import randint, seed
from hashlib import sha256
import communication


def _extended_gcd(a, b):
    x = 0
    last_x = 1
    y = 1
    last_y = 0
    while b != 0:
        quot = a // b
        a, b = b, a % b
        x, last_x = last_x - quot * x, x
        y, last_y = last_y - quot * y, y
    return last_x, last_y


def _divmod(num, den, p):
    inv, _ = _extended_gcd(den, p)
    return num * inv


def calcWeights(n, p):
    xs = [i + 1 for i in range(n)]
    terms = []
    for i in range(n):
        xj = xs[i]
        tempXs = xs[:i] + xs[i + 1:]
        wi = 1
        for j in range(0, n - 1):
            wi *= (xj - tempXs[j])
        terms.append(_divmod(1, wi, p) % p)
    return terms


def calcLinCombs(args, list1, list2):
    if len(list1) != len(list2):
        print("Length of lists were unequal!")
        exit()

    linComb = 0
    for i in range(len(list1)):
        linComb += list1[i] * list2[i]

    return linComb % args.prime


def genRandomValues(args, seedValue, numRandomValues):
    seed(seedValue)
    randomValues = [randint(0, args.prime - 1) for i in range(numRandomValues)]
    return randomValues


def calcCoeff(depth, xs, p):
    if depth == 0:
        return xs
    multiplicatives = []
    for i in range(len(xs) - depth):
        multiplicatives.extend([(xs[i] * el) % p for el in calcCoeff(depth - 1, xs[i + 1:], p)])
    return multiplicatives


def pointsOnPolynomial(args, shamirShares, polynomial):
    for i in range(args.thresholdShamir, args.numPlayers):
        yValue = 0
        for j, coef in enumerate(polynomial):
            yValue += coef * ((i + 1)**j)
        yValue %= args.prime
        if yValue != shamirShares[i]:
            return False
    return True


def reconstructShamir(args, shamirShares):
    weigts = calcWeights(args.thresholdShamir, args.prime)
    coeffs = [[] for i in range(args.thresholdShamir)]
    xs = [i + 1 for i in range(args.thresholdShamir)]
    for i in range(args.thresholdShamir):
        tempXs = xs[:i] + xs[i + 1:]
        for j in range(args.thresholdShamir):
            if j > 0:
                tempCoeffs = calcCoeff(j - 1, tempXs, args.prime)
                tempCoeffsSum = sum(tempCoeffs)
                coeff = (-1)**j * tempCoeffsSum
            else:
                coeff = 1
            coeffs[j].append((coeff * weigts[i]) % args.prime)

    reconstructedPoly = [calcLinCombs(args, shamirShares[:args.thresholdShamir], coef) for coef in coeffs]
    reconstructedPoly.reverse()
    reconstructedC = reconstructedPoly[0]

    # Verify whether all points are on the same polynomial
    if not pointsOnPolynomial(args, shamirShares, reconstructedPoly):
        print("Shamir shares were not defined on a k degree polynomial")
        exit()

    return reconstructedC, coeffs[-1]


def efficientReconstructShamir(args, shamirShares, shamirReconstructTerms):
    blindingValues = calcLinCombs(args, shamirShares, shamirReconstructTerms)
    return blindingValues


def secureOpen(args, payload):
    hashedPayload = hashObject(payload)
    hashes = communication.sendAndReceive(args, hashedPayload, True, False, False)
    data = communication.sendAndReceive(args, payload, True, False, False)
    for i in range(len(hashes)):
        if hashObject(data[i]) != hashes[i]:
            print("Secure open failed!")
    return data


def partiallyOpen(args, payload):
    return communication.sendAndReceive(args, payload, False, False, True)


def generateShamirShares(args, blindingSharesSPDZ):
    publicIDs = [[1] + [(i + 1)**j for j in range(1, args.thresholdShamir)] for i in range(args.numPlayers)]

    coefficients = [[blindingSharesSPDZ[i][0]] + [randint(0, args.prime - 1)
                                                  for j in range(1, args.thresholdShamir)] for i in range(args.numBlinds)]
    points = [[calcLinCombs(args, publicIDs[i], coefficients[j]) for j in range(args.numBlinds)]
              for i in range(args.numPlayers)]
    return points


def exchShamirShares(args, shamirShares):
    return communication.sendAndReceive(args, shamirShares, False, False, False)


def addShamirShares(args, shamirShares):
    addedShares = [sum(shares) % args.prime for shares in list(zip(*shamirShares))]
    return addedShares


# PROTOCOL ShareShamir
def shareShamir(args, blindingSharesSPDZ):
    # Step 1a
    shamirShares = generateShamirShares(args, blindingSharesSPDZ)
    receivedShares = exchShamirShares(args, shamirShares)

    # Step 2a
    shamirShares = addShamirShares(args, receivedShares)

    return shamirShares


# PROTOCOL MACCheck
def MACCheck(args, partOpenValues, spdzShares, MACvalue):
    # STEP 1a
    if len(partOpenValues) > 1:
        # STEP 1ai
        ownSeed = randint(0, args.prime - 1)
        # STEP 1aii
        seeds = secureOpen(args, ownSeed)
        # STEP 1aiii
        seed = sum(seeds) % args.prime
        # STEP 1aiiii
        randValues = genRandomValues(args, seed, len(partOpenValues))
    # STEP 1b
    else:
        # STEP 1bi
        randValues = [1]

    # STEP 2a
    combPartialValue = calcLinCombs(args, partOpenValues, randValues)

    # STEP 2b
    combSPDZ = [calcLinCombs(args, [share[0] for share in spdzShares], randValues),
                calcLinCombs(args, [share[1] for share in spdzShares], randValues)]

    # STEP 2c
    sigma = (combSPDZ[1] - combPartialValue * MACvalue) % args.prime

    # STEP 3a
    data = secureOpen(args, sigma)

    # STEP 3b
    sigmaTotal = sum(data) % args.prime
    if not sigmaTotal == 0:
        print("MACCheck failed, abort protocol")
        exit()


def hashObject(objectToHash):
    m = sha256()
    m.update(str(objectToHash).encode('utf-8'))
    return str(m.digest())


def commitPhase(args, shamirShares):
    # STEP 1a
    ownRandomValueCommitment = randint(0, 2**256)

    # STEP 1b
    ownCommitment = hashObject(shamirShares + [ownRandomValueCommitment])

    # STEP 1c
    allCommitments = communication.sendAndReceive(args, ownCommitment, True, True, False)

    return allCommitments, ownRandomValueCommitment


def challengePhase(args):
    # STEP 2a
    ownSeed = randint(0, args.prime - 1)
    # STEP 2b
    seeds = secureOpen(args, ownSeed)
    # STEP 2c
    seedValue = sum(seeds) % args.prime

    # STEP 2d
    randomValues = genRandomValues(args, seedValue, args.numBlinds)

    return randomValues


def responsePhase(args, randomValues, shamirShares, SPDZShares, MACshare):
    # STEP 3a
    ownRandLinCombSH = calcLinCombs(args, shamirShares, randomValues)

    # STEP 3b
    randLinCombSH = secureOpen(args, ownRandLinCombSH)

    # STEP 3c
    c, shamirReconstructTerms = reconstructShamir(args, randLinCombSH)

    # STEP 3d
    randLinCombSPDZ = [calcLinCombs(args, [share[0] for share in SPDZShares], randomValues),
                       calcLinCombs(args, [share[1] for share in SPDZShares], randomValues)]

    # STEP 3e
    MACCheck(args, [c], [randLinCombSPDZ], MACshare)

    return randLinCombSH, shamirReconstructTerms


# PROTOCOL GenValData
def genValData(args, shamirShares, SPDZShares, MACshare):
    # STEP 1
    validationHashes, randomValueCommitment = commitPhase(args, shamirShares)

    # STEP 2
    randomValues = challengePhase(args)

    # STEP 3
    randLinCombSH, shamirReconstructTerms = responsePhase(args, randomValues, shamirShares, SPDZShares, MACshare)

    return validationHashes, randomValueCommitment, randomValues, randLinCombSH, shamirReconstructTerms


def openShamirShares(args, shamirShares, randomValueCommitment):
    ownCommitmentInput = shamirShares + [randomValueCommitment]
    allCommitmentsInput = communication.sendAndReceive(args, ownCommitmentInput, True, True, False)
    return allCommitmentsInput


# PROTOCOL FairBlinds
def fairBlinds(args, shamirShares, randomValueCommitment, commitments, randomValues, randLinCombsSH, shamirReconstructTerms):
    # Step 1a
    allCommitmentsInput = openShamirShares(args, shamirShares, randomValueCommitment)

    # Step 2a
    omega = []
    # Step 2b
    hashedShares = [hashObject(shares) for shares in allCommitmentsInput]
    for i, commitment in enumerate(commitments):
        if commitment != hashedShares[i]:
            omega.append(i + 1)

    # STEP 2c
    linearCombsSH = [calcLinCombs(args, commitmentInput[:-1], randomValues) for commitmentInput in allCommitmentsInput]
    for i, linearCombSH in enumerate(linearCombsSH):
        if i + 1 not in omega and linearCombSH != randLinCombsSH[i]:
            omega.append(i + 1)

    # STEP 2d
    if len(omega) >= args.thresholdShamir:
        print("Too many participants deviated from the protocol!")
        exit()

    # STEP 3a - Reconstruct the blinding values
    blindingValues = [efficientReconstructShamir(args, shares[:args.thresholdShamir], shamirReconstructTerms)
                      for shares in list(zip(*allCommitmentsInput))[:-2]]
    return blindingValues
