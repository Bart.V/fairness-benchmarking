import asyncore
import json
import time
import argparse
import sys
import os
from timeit import default_timer as timer
import asyncio


def initialiseDataFolder():
    if not os.path.isdir(DATA_FOLDER):
        os.mkdir(DATA_FOLDER)


def parse_arguments(parser):
    global HOST, PORT, DATA_FOLDER

    arguments = parser.parse_args()

    HOST = arguments.Host
    PORT = arguments.Port
    DATA_FOLDER = arguments.Data_Folder


def create_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "Host", help="Host to listen to for incoming connections", type=str)
    parser.add_argument(
        "Port", help="Port to listen to for incoming connections", type=int)
    parser.add_argument(
        "Data_Folder", help="Data folder in which to write incoming data", type=str)

    return parser


def decodeIncoming(data):
    return json.loads(data)


def encodeOutgoing(reply):
    return json.dumps(reply).encode()


def writeReceivedData(data):
    while True:
        try:
            step = data['Step']
            sender = data['Sender']

            fileName = DATA_FOLDER + str(step) + str(sender)
            with open(fileName, 'w') as file:
                file.write(json.dumps(data))
            return
        except:
            print("Received data could not be written to file")


def parse_data(data):
    index = data.rfind('}') + 1
    shortenedData = data[:index]
    while index > 0:
        try:
            decodedData = decodeIncoming(shortenedData)
            writeReceivedData(decodedData)
            return parse_data(data[index:])
        except:
            pass

        index = shortenedData[:-1].rfind('}') + 1
        shortenedData = data[:index]
        if len(shortenedData) == 0:
            return data

    return data


class ReceiveData(asyncio.Protocol):

    def connection_made(self, transport):
        self.data = ''

    def data_received(self, data):
        remainingData = parse_data(self.data + data.decode())
        self.data = remainingData

async def main():
    loop = asyncio.get_running_loop()
    server = await loop.create_server(ReceiveData, HOST, PORT)
    await server.serve_forever()


if __name__ == '__main__':
    parser = create_argparse()
    parse_arguments(parser)
    initialiseDataFolder()
    asyncio.run(main())
