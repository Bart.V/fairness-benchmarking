from hashlib import sha256
import socket
import json
import argparse
import subprocess
import sys
import os
from random import randint, seed


def splitValue(value, shares, p):
    randomShares = [randint(0, p - 1) for i in range(shares - 1)]
    correctingShare = [(value - sum(randomShares)) % p]
    return randomShares + correctingShare


def create_random_spdz_sharings(MACs, numPlayers, numBlinds, numRuns, prime):

    shared_values = [[randint(0, prime - 1) for j in range(numBlinds)] for i in range(numRuns)]
    individual_values = [[splitValue(shared_values[i][j], numPlayers, prime) for j in range(numBlinds)] for i in range(numRuns)]
    individual_mac_tags = [[splitValue(shared_values[i][j] * MACs[i], numPlayers, prime)
                            for j in range(numBlinds)] for i in range(numRuns)]

    shared_random_sharings = []
    for i in range(numPlayers):
        shared_random_sharings.append([[[individual_values[k][l][i], individual_mac_tags[k][l][i]]
                                        for l in range(numBlinds)] for k in range(numRuns)])

    return shared_random_sharings, shared_values


def calculateSharedRandomness(numPlayers, numBlinds, numRuns, consumedRandomness, prime):
    # Include the sacrifice blind in generation
    numBlinds += 1

    MACs = [randint(0, prime - 1) for i in range(numRuns)]
    allRunsAllMACshares = [splitValue(MACs[i], numPlayers, prime) for i in range(numRuns)]

    blinding_values, _ = create_random_spdz_sharings(MACs, numPlayers, numBlinds, numRuns, prime)
    output_shares, output_values = create_random_spdz_sharings(MACs, numPlayers, numBlinds, numRuns, prime)
    consumed_randomness, consumed_values = create_random_spdz_sharings(MACs, numPlayers, consumedRandomness, numRuns, prime)

    gen_shared_randomness = []
    for i in range(numPlayers):
        randomness = {"MACshare": [allRunsAllMACshares[j][i]
                                   for j in range(numRuns)], "AdditiveBlinds": blinding_values[i], "SharedOutputs": {"OutputShares": output_shares[i]}, "ConsumedRandomness": {"Sharings": consumed_randomness[i], "Values": consumed_values}}
        gen_shared_randomness.append(randomness)

    return gen_shared_randomness


def write_shared_randomness(shared_randomness, numPlayers, prime, out_directory):
    for i in range(numPlayers):
        with open(out_directory + "player_" + str(i + 1) + '_' + str(prime), 'w') as file:
            file.write(json.dumps(shared_randomness[i]))


def initialise_shared_randomness(numPlayers, numBlinds, numRuns, consumedRandomness, prime, out_directory):
    shared_randomness = calculateSharedRandomness(numPlayers, numBlinds, numRuns, consumedRandomness, prime)
    write_shared_randomness(shared_randomness, numPlayers, prime, out_directory)
