import json
import argparse
from timeit import default_timer as timer
from random import randint, seed
import communication
import computation
import subprocess
import re

input_folder = './input/'
output_folder = './output/'


def str2bool(v):
    if v.lower() == 'true':
        return True
    else:
        return False


def create_argparse():
    parser = argparse.ArgumentParser()

    parser.add_argument("player", help="Name of the participant", type=str)
    parser.add_argument("numPlayers", help="Amount of participants", type=int)
    parser.add_argument("thresholdShamir", help="Reconstruction threshold for Shamir shares", type=int)
    parser.add_argument("numBlinds", help="Amount of blinding values to generate", type=int)
    parser.add_argument("numRuns", help="Amount of runs to benchmark", type=int)
    parser.add_argument("prime", help="Size of the Prime field", type=int)
    parser.add_argument("fairReveal", help="Reveal the output values fairly (use our scheme)", type=str2bool)
    parser.add_argument("Host", help="Host to listen to for incoming connections", type=str)
    parser.add_argument("Port", help="Port to listen to for incoming connections", type=int)
    parser.add_argument("Data_Folder", help="Data folder in which to write incoming data", type=str)
    parser.add_argument("networkDelay", help="Artiticial delay of network traffic in milliseconds", type=int)
    parser.add_argument("networkBandwith", help="Artificial throttling of network bandwith in kbit", type=int)

    return parser


def args(parser):
    args = parser.parse_args()
    args.numBlinds = args.numBlinds + 1
    return args


def coordinateRuns(args):
    communication.sendAndReceive(args, {"Ready": "Yes"}, True, False, False)
    communication.sendAndReceive(args, {"Ready": "Yes"}, True, False, False)


def loadSharedRandomness(args):
    with open(input_folder + str(args.player) + '_' + str(args.prime), 'r') as file:
        data = json.loads(file.read())

    return data


def write_running_times(args, data):
    output_file = output_folder + str(args.player)
    with open(output_file, 'w+') as file:
        file.write(json.dumps(data))


# PROTOCOl FairOutputs
def fairOutputs(args, MACshare, blindingSharesSPDZ, outputShares, consumedRandValues, consumedRandShares):
    coordinateRuns(args)

    timings = {}
    data = {}
    beginSend = sendBytes()
    beginTime = timer()
# Preprocessing phase ----------------------------------------------
    # Protocol ShareShamir - Transforming the SPDZ-sharings into Shamir sharings
    shamirShares = computation.shareShamir(args, blindingSharesSPDZ)

    # Protocol GenValData - Generating validation data for the Shamir sharings
    commitments, randomValueCommitment, randomValues, randLinCombSH, shamirReconstructTerms = computation.genValData(
        args, shamirShares, blindingSharesSPDZ, MACshare)
    timings['Offline Phase'] = timer() - beginTime
    data['Offline Phase'] = sendBytes() - beginSend

# Online phase -----------------------------------------------------
    beginSend = sendBytes()
    beginTime = timer()
    # Step 1a
    computation.MACCheck(args, consumedRandValues, consumedRandShares, MACshare)
    timings['Verify Computation'] = timer() - beginTime
    data['Verify Computation'] = sendBytes() - beginSend

    # Step 2a - Blinding the output values
    beginSend = sendBytes()
    beginTime = timer()
    blindedOutputShares = [[(outputShares[i][0] + blindingSharesSPDZ[i][0]) % args.prime,
                            (outputShares[i][1] + blindingSharesSPDZ[i][1]) % args.prime] for i in range(args.numBlinds)]

    # Step 2b - Opening the blinded output values
    blindedOutputValues = computation.partiallyOpen(args, [[blindedOutputShares[i][0] for i in range(args.numBlinds)]])

    # Step 2c - Verifying the output values
    computation.MACCheck(args, blindedOutputValues, blindedOutputShares, MACshare)

    # Step 3a - Verifying the generated validation data and opening the blinding values
    blindingValues = computation.fairBlinds(args, shamirShares, randomValueCommitment, commitments,
                                            randomValues, randLinCombSH, shamirReconstructTerms)

    # Step 3b - Unblinding the blinded output values
    unblindedOutputValues = [(blindedOutputValues[i] - blindingValues[i]) % args.prime for i in range(args.numBlinds - 1)]
    timings['Fair Online Reveal'] = timer() - beginTime
    data['Fair Online Reveal'] = sendBytes() - beginSend

    return timings, data


# PROTOCOL StandardReveal
def standardReveal(args, MACshare, blindingSharesSPDZ, outputShares, consumedRandValues, consumedRandShares):
    coordinateRuns(args)

    timings = {}
    data = {}
    # STEP 1a
    beginSend = sendBytes()
    beginTime = timer()
    computation.MACCheck(args, consumedRandValues, consumedRandShares, MACshare)
    timings['Verify Computation'] = timer() - beginTime
    data['Verify Computation'] = sendBytes() - beginSend

    # Step 2a
    beginSend = sendBytes()
    beginTime = timer()
    outputValuesShares = [outputValue[0] for outputValue in outputShares]
    outputValues = computation.partiallyOpen(args, [outputValuesShares])

    # Step 2b
    computation.MACCheck(args, outputValues, outputShares, MACshare)
    timings['Standard Online Reveal'] = timer() - beginTime
    data['Standard Online Reveal'] = sendBytes() - beginSend

    return timings, data


def run_reveal(args, MACshare, AdditiveBlinds, outputShares, consumedValues, consumedShares):
    seed(timer() * 1000000000)
    if args.fairReveal:
        times = fairOutputs(args, MACshare, AdditiveBlinds, outputShares, consumedValues, consumedShares)
    if not args.fairReveal:
        times = standardReveal(args, MACshare, AdditiveBlinds, outputShares, consumedValues, consumedShares)
    return times


def sendBytes():
    sendData = subprocess.run(["iptables -n -L -v -x"], shell=True, capture_output=True).stdout.decode().split('\n')[-2]
    numberBytes = int(re.findall('[0-9]+', sendData)[1])
    return numberBytes


def run_complete_setup(args):
    subprocess.run(["iptables -A OUTPUT ! -d " + args.player + " -p tcp"], shell=True)

    # Setup everything needed to communicate
    communication.startServer(args)
    communication.initialise_network(args)
    communication.initialiseSockets(args)

    # Load the player-specific shared randomness
    sharedRandomness = loadSharedRandomness(args)

    # Used to debug amount of communicated data
    runData = {}
    beginTime = timer()
    for i in range(args.numRuns):
        times, data = run_reveal(args,
                                 sharedRandomness['MACshare'][i],
                                 sharedRandomness['AdditiveBlinds'][i],
                                 sharedRandomness['SharedOutputs']['OutputShares'][i],
                                 sharedRandomness['ConsumedRandomness']['Values'][i],
                                 sharedRandomness['ConsumedRandomness']['Sharings'][i])
        runData["run_" + str(i + 1)] = {"Run Times": times, "Send Data": data}

    print("Finished all " + str(args.numRuns) + " protocol runs")
    print(timer() - beginTime)

    communication.closeConnections()

    write_running_times(args, runData)


if __name__ == '__main__':
    parser = create_argparse()
    args = args(parser)
    run_complete_setup(args)
