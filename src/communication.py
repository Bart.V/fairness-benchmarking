import socket
import json
import subprocess
import traceback
from os import path
from hashlib import sha256
import gevent

kingPlayer = 'player_1'
# Indexes of the steps
stepCounter = 0


def startServer(args):
    subprocess.Popen(["python", "./src/server.py", str(args.Host), str(args.Port), str(args.Data_Folder)])


def initialise_network(args):
    limitNetwork(args)
    initialiseSockets(args)


def closeConnections():
    for s in openSockets.values():
        s.close()


def readReceivedData(args, step, sender):
    fileName = args.Data_Folder + str(step) + str(sender)

    if not path.isfile(fileName):
        return None

    while True:
        try:
            with open(fileName) as file:
                return json.load(file)
        except:
            pass


def waitOnData(args, numCommParties, step):
    receivedData = [0 for i in range(numCommParties)]
    remaingReceive = [i + 1 for i in range(numCommParties)]
    while len(remaingReceive) > 0:
        for remaining in remaingReceive:
            readData = readReceivedData(args, step, 'player_' + str(remaining))
            if readData is not None:
                receivedData[remaining - 1] = readData['Payload']
                remaingReceive.remove(remaining)

    return receivedData


def handleSending(args, numCommParties, step, payloads, extendPayload):
    if extendPayload:
        payloads = [payloads for i in range(args.numPlayers)]

    threads = [0 for i in range(numCommParties)]
    for i, payload in enumerate(payloads):
        threads[i] = gevent.spawn(sendData, args, 'player_' + str(i + 1), step, payload)

    gevent.joinall(threads)


def hashObject(objectToHash):
    m = sha256()
    m.digest()
    m.update(str(objectToHash).encode('utf-8'))
    return str(m.digest())


def sendAndReceive(args, payload, extendPayload, broadcast, partialOpen):
    global stepCounter
    stepCounter += 1

    if partialOpen:
        handleSending(args, 1, stepCounter, payload, extendPayload)
        if args.player == kingPlayer:
            data = waitOnData(args, args.numPlayers, stepCounter)
            addedData = [sum([data[i][j] % args.prime for i in range(len(data))]) for j in range(len(data[0]))]
            # print(len(addedData))
            handleSending(args, args.numPlayers, stepCounter, addedData, True)
            data = addedData
        else:
            data = waitOnData(args, 1, stepCounter)[0]

    else:
        handleSending(args, args.numPlayers, stepCounter, payload, extendPayload)
        data = waitOnData(args, args.numPlayers, stepCounter)

    if broadcast:
        hashedData = hashObject(data)
        receivedData = sendAndReceive(args, hashedData, True, False, False)
        for receivedHash in receivedData:
            if hashedData != receivedHash:
                print("Broadcast went wrong")
                exit()

    return data


def initialiseSockets(args):
    global openSockets
    openSockets = {}

    alreadyPrinted = False
    receivers = ['player_' + str(i + 1) for i in range(args.numPlayers)]
    for receiver in receivers:
        connected = False
        while not connected:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                HOST = receiver                             # The server's hostname or IP address
                PORT = args.Port                            # The port used by the server
                s.connect((HOST, PORT))
                openSockets[receiver] = s
                connected = True
            except Exception as e:
                s.close()
                if not alreadyPrinted:
                    print(e)
                alreadyPrinted = True


def sendData(args, receiver, step, payload):
    global openSockets
    alreadyPrinted = False
    while True:
        try:
            s = openSockets[receiver]
            message = {"Sender": args.player, "Step": step, "Payload": payload}
            encodedMessage = json.dumps(message).encode()
            s.sendall(encodedMessage)
            return
        except Exception as e:
            if not alreadyPrinted:
                print(traceback.format_exc())
            alreadyPrinted = True


def limitNetwork(args):
    if args.networkDelay > 0 or args.networkBandwith > 0:
        subprocess.run(["tc qdisc add dev eth0 root netem delay " + str(args.networkDelay) +
                        "ms rate " + str(args.networkBandwith) + "kbit"], shell=True)
