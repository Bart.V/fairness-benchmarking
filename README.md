### General
This repository aims to analyze the performance of guaranteeing fairness for SPDZ-based protocols by benchmarking and comparing the fair and non-fair reveal phase as described in the unpublished paper "Extending the Security of SPDZ with Fairness". To provide an honest comparison between the two, both are implemented in Python. 

## Dependencies
To run the example you need to have the following installed
* Python3 (Tested on Python3.8)
* Docker
* Docker-compose


### Run
The following command can be run from the root of this repository:
```
python3 benchmarking.py
```

### Acknowledgements
This paper is based on the Master Thesis “Making SPDZ Fair” by Bart Veldhuizen (2020) as a requirement for the master Computing Science at the Radboud University, Nijmegen, NL. All research related to this thesis was performed at the Cyber Security and Robustness department, TNO, The Hague, NL. This work was further supported by the TNO Early Research Programme “Next Generation Cryptography”. The work of L. Kohl was carried out in the CWI Cryptology group, Amsterdam, supported by the NWO Talent Programme Veni (VI.Veni.222.348) and the NWO Gravitation Project QSC.
