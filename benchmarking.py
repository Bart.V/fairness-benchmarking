import subprocess
import itertools
import json
import os
from pathlib import Path
import src.rand_generator

# General construction layout of the docker files
dockerComposeFile = 'Docker.Compose'

header = """version: '2.2'
services:
"""

playerBuild = """  $Player:
    cpus: 2
    build:
      context: .
      dockerfile: docker/player.Dockerfile
    cap_add:
      - NET_ADMIN
      - NET_RAW
    command: $Commands
    volumes:
      - "./data/output:/output"
      - "./data/input:/input"
    container_name: $Player
"""

BASE_PATH = str(Path(__file__).parent.absolute())
DATA_FOLDER = BASE_PATH + '/data/'
RUN_DATA_FILE = DATA_FOLDER + 'output.data'
INPUT_FOLDER = DATA_FOLDER + 'input/'
OUTPUT_FOLDER = DATA_FOLDER + 'output/'

SOURCE_FOLDER = '/src/'
COMMUNICATION_FOLDER = '/comm/'

HOST = ''
PORT = 7777

possPlayers = [3, 4, 5]
possBlinds = [1, 500, 1000, 1001]
possPrimes = [2**128 - 159]
possNetworkDelay = [False, True]
possFairReveal = [True, False]

consumedRandomness = 2

runsPerSimulation = 100

latencyWAN = "50"                   # Latency (in milliseconds) to use in WAN setting
bandwithWAN = "51200"               # Maximum bandwith (in kbits) to use in WAN setting

verbose = False


def create_docker_configuration(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay):
    configuration = generate_docker_configuration(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay)
    write_docker_configuration(configuration)


def generate_docker_configuration(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay):
    arguments = combineArguments(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay)
    dockerData = header
    for i in range(numPlayers):
        playerData = playerBuild
        currPlayer = 'player_' + str(i + 1)
        argumentStr = str([currPlayer] + arguments)

        playerData = playerData.replace('$Player', currPlayer)
        playerData = playerData.replace('$Commands', argumentStr)
        dockerData += playerData
    return dockerData


def write_docker_configuration(data):
    with open(dockerComposeFile, 'w') as file:
        file.write(data)


def combineArguments(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay):
    thresholdShamir = numPlayers - int(numPlayers / 2)

    arguments = []
    arguments.append(str(numPlayers))
    arguments.append(str(thresholdShamir))
    arguments.append(str(numBlinds))
    arguments.append(str(numRuns))
    arguments.append(str(prime))
    arguments.append(str(fairReveal))
    arguments.append(str(HOST))
    arguments.append(str(PORT))
    arguments.append(str(COMMUNICATION_FOLDER))

    if networkDelay:
        arguments.append(str(latencyWAN))
        arguments.append(str(bandwithWAN))
    else:
        arguments.append(str(0))
        arguments.append(str(0))

    return arguments


def resetDocker():
    subprocess.run(["docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)"],
                   shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def load_output(numPlayers):
    runData = {}
    output_files = [f for f in os.listdir(OUTPUT_FOLDER) if os.path.isfile(os.path.join(OUTPUT_FOLDER, f))]
    if len(output_files) != numPlayers:
        print("Run data of participants could not be read")
        exit()
    for out_file in output_files:
        with open(os.path.join(OUTPUT_FOLDER, out_file), 'r') as file:
            runData[out_file] = json.loads(file.read())

    return runData


def resetFiles():
    for f in os.listdir(INPUT_FOLDER):
        if os.path.isfile(os.path.join(INPUT_FOLDER, f)):
            os.remove(os.path.join(INPUT_FOLDER, f))

    for f in os.listdir(OUTPUT_FOLDER):
        if os.path.isfile(os.path.join(OUTPUT_FOLDER, f)):
            os.remove(os.path.join(OUTPUT_FOLDER, f))

    if os.path.isfile(dockerComposeFile):
        os.remove(dockerComposeFile)


def run_docker_configuration(numPlayers):
    # Reset the docker and optionally traffic forwarding service
    resetDocker()

    # Run docker-compose on the dockerfile
    subprocess.run(["docker-compose", "-f", str(dockerComposeFile),
                    "up", "--build", "--abort-on-container-exit"], capture_output=not verbose)
    runData = load_output(numPlayers)
    return runData


def run_single_simulation(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay):
    resetFiles()

    src.rand_generator.initialise_shared_randomness(numPlayers, numBlinds, numRuns, consumedRandomness, prime, INPUT_FOLDER)

    create_docker_configuration(numPlayers, numBlinds, numRuns, prime, fairReveal, networkDelay)

    times = run_docker_configuration(numPlayers)

    return times


def process_data(numPlayers, numBlinds, prime, fairReveal, networkDelay, run_data):
    data_dict = {"numPlayers": numPlayers, "numBlinds": numBlinds, "consumedRandomness": consumedRandomness,
                 "prime": prime, "fairReveal": fairReveal, "networkDelay": networkDelay, "data": run_data}

    with open(RUN_DATA_FILE, 'a+') as file:
        file.write(json.dumps(data_dict) + "\n")


def initialiseFolders():
    if not os.path.isdir(DATA_FOLDER):
        os.mkdir(DATA_FOLDER)
    if not os.path.isdir(INPUT_FOLDER):
        os.mkdir(INPUT_FOLDER)
    if not os.path.isdir(OUTPUT_FOLDER):
        os.mkdir(OUTPUT_FOLDER)


def run_all_simulations():
    initialiseFolders()
    allCombinationsOfParamters = itertools.product(possPlayers,
                                                   possBlinds,
                                                   possPrimes,
                                                   possFairReveal,
                                                   possNetworkDelay)

    for numPlayers, numBlinds, prime, fairReveal, networkDelay in allCombinationsOfParamters:
        run_data = run_single_simulation(numPlayers, numBlinds, runsPerSimulation, prime, fairReveal, networkDelay)
        process_data(numPlayers, numBlinds, prime, fairReveal, networkDelay, run_data)


if __name__ == '__main__':
    run_all_simulations()
