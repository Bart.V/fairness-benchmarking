FROM python:3 AS pythonenvironment
RUN pip install gevent 
RUN apt update && apt install iproute2 dnsutils tcpdump iptables -y

FROM pythonEnvironment AS player
ADD src/player.py /src/
ADD src/server.py /src/
ADD src/communication.py /src/
ADD src/computation.py /src/
ENTRYPOINT [ "python3", "-u", "./src/player.py" ]
